=head1 NAME

java-deploy-webapp - Deploy a Java web application to a web container

=head1 SYNOPSIS

B<java-deploy-webapp> [B<-hv>] [B<-c> I<container>[,I<container>,...]]
    [B<-o> I<path>] I<webapp> [I<webapp> ...]

B<java-deploy-webapp> B<-l> [I<webapp> ...]

B<java-deploy-webapp> B<-u> [B<-c> I<container>[,I<container>,...]]
    I<webapp> [I<webapp> ...]

B<java-deploy-webapp> B<-r> [B<-c> I<container>[,I<container>,...]]
    I<webapp> [I<webapp> ...]

=head1 DESCRIPTION

B<java-deploy-webapp> deploys a Java web application into a web container,
updates an existing deployed web application, or removes (undeploys) a web
application.

The default action is to deploy the named web application.  To do this,
B<java-deploy-webapp> constructs a WAR file by combining the files in
F</usr/share/java/webapps/I<webapp>> and F</etc/java/webapps/I<webapp>>
into a unified directory tree.  Files found in the second path override
files found in the first path if files with the same name are present in
both directories.  It then builds a WAR file from that combined directory
tree and deploys it into a container.

If only one container recognized by B<java-deploy-webapp> is installed,
the deploy action will deploy the web application into that container.
Otherwise, the default action will deploy into the "best" web container
installed, based on a priority list in B<java-deploy-webapp>.  This
default behavior can be overridden by listing one or more containers on
the command line with the B<-c> option.

The B<-l> flag lists all web containers into which the given web
application is deployed.

The B<-u> flag says to update an existing web application instead of
deploy it.  This rebuilds the application WAR file as described above and
then deploys it to every web container to which it is currently deployed.
This list of containers can be overridden with the B<-c> option.

The B<-r> flag says to remove (undeploy) the web application.  By default,
it is removed from all web containers to which it is deployed.  This list
of containers can be overridden with the B<-c> option.

=head1 OPTIONS

=over 4

=item B<--container>=I<container>[,...], B<-c> I<container>[,...]

Rather than acting on the default preferred container (for deploy) or on
all containers to which the web application is deployed (for other
actions), act only on the listed containers.  The argument may be a
single container or multiple containers separated by commas.  The
following containers are currently supported:

=over 4

=item *

jetty (Jetty)

=item *

tomcat6 (Tomcat 6)

=back

=item B<--help>, B<-h>

Print a short summary of the usage of B<java-deploy-webapp> and exit.  All
other options are ignored and no changes are made to web applications on
the system.

=item B<--list>, B<-l>

List all web containers into which the given web application is deployed.
If no web applications are given on the command line, lists the web
containers for all web applications known to B<java-deploy-webapp>.  No
changes are made to web applications on the system.

=item B<--output> I<directory>, B<-o> I<directory>

Rather than deploying the web application in a container, generate the WAR
file that would have been deployed and save that WAR file in the specified
output directory.

=item B<--remove>, B<-r>

Remove the given web applications from all web containers in which they're
currently deployed, or only those web containers listed with B<-c> if that
option was given.

=item B<--update>, B<-u>

Update the given web applications from their source files in
F</usr/share/java/webapps> and F</etc/java/webapps> and redeploy them to
all web containers in which they're currently deployed, or only those web
containers listed with B<-c> if that option was given.

=back

=head1 FILES

=over 4

=item F</var/lib/java-deploy-webapp>

This directory is used to record the list of web applications known to
this tool and the list of web containers into which they've been
deployed.

=back

=head1 BUGS

Deploying and updating possibly should be the same action, since that
would make writing a postinst maintainer script easier.

There's no way in this interface to remove all web applications deployed
in a particular container.  The broader question is what should this tool
do (if anything) when a web container that has deployed applications is
removed from the system?  There should probably at least be some way of
updating the deployment state so that it's not inconsistent with what's on
the system.

The priority order of web containers needs to be determined.

The B<-c> flag is probably not sufficient to handle web applications that
require a particular set of containers.  There will probably need to be
another flag to specify the supported containers as input to the choice of
container for deployment (and so that a proper error message can be given
if none of those containers are installed, although package dependencies
should normally take care of that).
