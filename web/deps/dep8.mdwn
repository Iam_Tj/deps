[[!meta title="DEP-8: automatic as-installed package testing"]]

    Title: autopkgtest - automatic as-installed package testing
    DEP: 8
    State: DRAFT
    Date: 2012-06-25
    Drivers: Ian Jackson <ijackson@chiark.greenend.org.uk>,
     Iustin Pop <iustin@debian.org>,
     Stefano Zacchiroli <zack@debian.org>
    URL: http://dep.debian.net/deps/dep8
    Source: https://salsa.debian.org/dep-team/deps/-/blob/master/web/deps/dep8.mdwn
    License:
     Copying and distribution of this file, with or without modification,
     are permitted in any medium without royalty provided the copyright
     notice and this notice are preserved.
    Abstract:
     Establish a standard interface to define and run "as-installed" tests of
     packages, i.e. the testing of packages in a context as close as possible
     to a Debian system where the packages subject to testing are properly
     installed.

[[!toc ]]


# Introduction

In the working.

In the meantime you can check the
[current specification](https://salsa.debian.org/ci-team/autopkgtest/raw/master/doc/README.package-tests.rst),
as implemented in [autopkgtest](http://packages.debian.org/sid/autopkgtest).

