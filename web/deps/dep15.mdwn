[[!meta title="DEP 15: Reserved namespace for DD-approved non-maintainer changes"]]

    Title: Reserved namespace for DD-approved non-maintainer changes
    DEP: 15
    State: DRAFT
    Date: 2017-06-07
    Drivers: Sean Whitton <spwhitton@spwhitton.name>
     Ian Jackson <ijackson@chiark.greenend.org.uk>
    URL: http://dep.debian.net/deps/dep15
    Source: https://salsa.debian.org/dep-team/deps/-/blob/master/web/deps/dep15.mdwn
    License:
     Copying and distribution of this file, with or without modification,
     are permitted in any medium without royalty provided the copyright
     notice and this notice are preserved.
    Abstract:
     Reserved namespace in version control tools for changes to the
     package that have been approved by someone with the technical
     ability to upload the package, but not necessarily the package's
     maintainer.


Introduction
------------

This DEP reserves a namespace in version control tools for changes
that are

1. considered technically appropriate for an NMU by someone with the
   technical ability to NMU the package; but are

2. not considered socially appropriate for an NMU right now, or such
   that it is not clear in which maintainer upload the changes should
   be included.

Motivation
----------

We can think of the Debian Archive as a version control system, where
an upload of a new package version to ftp-master corresponds to making
a commit.  However, in contrast with tools like `git commit`,
uploading a package is a relatively expensive and potentially
disruptive operation.  The changelog must be finalised, an appropriate
version number must be chosen, and the package will be distributed to
users, so it must be established that the package maintainer does not
wish to include additional changes along with those currently listed.

Debian contributors frequently want to submit fixes and improvements
to packages which they do not maintain.  Sometimes, it is appropriate
to make an NMU, but very often the maintainer should be the one to
decide the upload in which the change is included.  In other words,
the contributor considers the fix technically correct and would
include it in, for example, a QA upload.  However, since the package
is under active maintainance, the contributor wants to avoid
interrupting the package maintainer's work by making an upload.

Currently, the only canonical way to submit such a fix is to post
patches to the Debian BTS.  However,

1. many non-maintainer contributors of fixes and improvements, and
   maintainers who review those changes, much prefer working with
   branches in a version control system instead of wrangling patches;
   and

2. it is very useful for a maintainer to know that a change has been
   approved by someone who has been trusted by the project with the
   technical ability to NMU the package, but it is cumbersome (and
   easy to neglect) to both make and verify PGP signatures on patch
   series submitted to the BTS.

Since many Debian source packages are maintained in version control
repositories on alioth, an alternative to submitting patches to the
BTS is to push a topic branch to the package's alioth repository, and
ask the maintainer to review and merge.  However,

1. those who have the ability to NMU the package often lack the
   ability to push to the repository on alioth;
   
2. since those who do not have the ability to NMU the package may push
   topic branches to alioth, again it is cumbersome for the maintainer
   to determine whether proposed changes have been approved by someone
   trusted by the project with the technical ability to upload the
   package; and
   
2. there is no standard namespace to indicate topic branches that fall
   into the category of changes technically appropriate for an NMU,
   but not socially appropriate for an NMU.
   
Namespace
---------

Branches named `next/foo`, where `foo` is a label for the change.  For
example, `next/std-ver-3.9.8` for patches to bring the package in
compliance with Policy version 3.9.8.

Publishing proposed changes
---------------------------

There must be a single canonical location for `next/foo` branches.
These branches must be writeable by exactly those capable of uploading
the package -- uploading DDs, and DMs with upload rights for the
package.

These branches should not be published under this name anywhere else.
A contributor who does not have the ability to upload the package
should publish their branch under a name outside of the `next/`
namespace, ask someone with the relevant upload rights to review it,
and push it to the canonical location.

Folding proposed changes into packages
--------------------------------------

Package maintainers can fetch and merge `next/` branches, or
cherry-pick from them.  When all the changes on a `next/` branch that
the maintainer intends to include in the package have been merged or
copied onto the master branch, the `next/` branch should be deleted.

Suggested implementation
------------------------

The [dgit repos](https://browse.dgit.debian.org/) already have most of
the required access control.  All that remains is adding support for
signed git pushes.  See [#848678](https://bugs.debian.org/848678).

Note that this does **not** involve any use of dgit on the client
side.  Branches would be updated with `git push --signed`, not `dgit
push`.

Criteria for success of this DEP
--------------------------------

In addition to consensus on the text,

1. implementation complete;

2. at least some contributors pushing `next/foo` branches; and

3. at least some maintainers merging `next/foo` branches into their
   uploads.

FAQs
----

Why is this separate from DEP-14?

DEP-14 describes branch and tag naming conventions that should be
followed in all copies/clones of a repository.  By contrast, this DEP
describes branches that should only exist in one of those
copies/clones.

Changes
-------

* 2017-07-10
  [ Sean Whitton ]
  * Add first FAQ regarding separation from DEP-14.
  * Generalise first criterion of success, so it doesn't refer
    exclusively to the suggested implementation on dgit repos.
* 2017-06-07:
  [ Sean Whitton ]
  * Initial proposal draft, based on an idea of Ian Jackson's, shared
    during a discussion we had regarding #848678.
